const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const babel = require('./webpack/babel');
const fonts = require('./webpack/fonts');
const json = require('./webpack/json');
const css = require('./webpack/css');
const images = require('./webpack/images');
const env = require('./webpack/env');
const extract_styles = require('./webpack/extractStyles');
const sass = require('./webpack/sass');
const dev_server = require('./webpack/devServer');
const html_webpack_plugin = require('html-webpack-plugin');
const clean_webpack_plugin = require('clean-webpack-plugin');
const uglify_js_plugin = require('uglifyjs-webpack-plugin');
const imagemin_plugin = require('imagemin-webpack-plugin').default;
const copy_webpack_plugin = require('copy-webpack-plugin');




let conf = merge([{


        entry: './src/index.js',

        output: {
            path: path.resolve(__dirname, 'dist'),
            filename: './static/js/bundle.js',
            publicPath: '/'
        },

        plugins: [
            new html_webpack_plugin({
                template: './src/index.html'
            }),
            new copy_webpack_plugin(
                [{
                    from: './src/assets/images',
                    to: './static/images'
                }], {
                    ignore: [{
                        // glob: 'svg/*.svg'
                    }]
                }
            ),

            new imagemin_plugin({
                test: /\.(png|gif|jpeg|jpg|svg|ico)$/i
            }),
        ],
    },
    babel(),
    fonts(),
    // json()
    images(),
    env()
]);

module.exports = (env, options) => {
    let production = options.mode === 'production';

    conf.devtool = production ? 'source-map' : 'cheap-eval-source-map';
    if (production) {
        return merge([
            conf,
            {
                plugins: [
                    new clean_webpack_plugin(['dist']),

                    new uglify_js_plugin({
                        sourceMap: true
                    }),

                    new webpack.LoaderOptionsPlugin({
                        minimize: true
                    })
                ]
            },
            // sass(),
            // css()
            extract_styles()

        ]);
    } else {
        return merge([
            conf,
            dev_server(),
            sass(),
            css()
        ]);
    }
};
