module.exports = function() {
    return {
        module: {
            rules: [{
                test: /\.(js|jsx)$/,
                exclude: /(node_modules|bower_components)/,
                use: [{
                    loader: 'babel-loader',
                    options: {
                        presets: ['env', 'react', 'react-hmre'],
                        plugins: [
                            "transform-object-rest-spread", "transform-class-properties"
                        ]
                    }
                }]
            }]
        },
        resolve: {
            extensions: ['.js', '.jsx'],
        },
    };
};
