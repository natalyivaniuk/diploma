module.exports = function() {
    return {
        module: {
            rules: [{
                test: /\.(png|gif|jpeg|jpg|ico)$/,
                use: [{
                        loader: 'file-loader',
                        options: {
                            name: '[path][name].[ext]',
                            context: 'src',
                            publicPath: '/'
                        }
                    },
                    'img-loader'
                ],
            }]
        }
    };
};
