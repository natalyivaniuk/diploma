module.exports = function() {
    return {
        module: {
            rules: [{
                test: /\.(woff|woff2|eot|ttf|otf|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[path][name].[ext]',
                        context: 'src',
                        outputPath: './static/fonts/'
                        // publicPath: '../'
                    }
                }],
            }]
        }
    };
};
