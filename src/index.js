import 'bootstrap/scss/bootstrap.scss';
import $ from 'jquery';
import Popper from 'popper.js';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import 'font-awesome/scss/font-awesome.scss';

import {
    Provider
} from 'react-redux';
import configureStore from './_store/configureStore'

import React from 'react';
import ReactDOM from 'react-dom';
import './styles.scss';
import AppComponent from './components/app.component';

const store = configureStore();


ReactDOM.render( <Provider store={store}>< AppComponent/></Provider>, document.getElementById('root'));
