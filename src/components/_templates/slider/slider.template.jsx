import React, { Component } from 'react';
import Slick from "react-slick";
import "./slider.template.scss";
import { Link } from "react-router-dom";

class SliderTemplate extends Component {

    settings = {
        dots: false,
        infinite: true,
        arrows: false,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        ...this.props.settings
    }

    showItems = () => {
        let template = null;
        switch (this.props.type) {
            case "news":
                template = this.props.data.map(item => {
                    const image = item.urlToImage;
                    const styles = {
                        backgroundImage: `url(${image})`
                    }
                    return (
                        <div key={item.source.name}>
                            <div className="featured_item">
                                <div className="featured_image"
                                    style={styles}>
                                </div>
                                <a href={`${item.url}`} target='_blank'>
                                    <div className="featured_caption">
                                        {item.title}
                                    </div>
                                </a>
                            </div>
                        </div>
                    )
                })
                break;

            default:
                template = null;
                break;
        }
        return template;
    }

    render() {
        return (
            <Slick {...this.settings}>
                {this.showItems()}
            </Slick>
        );
    }
}

export default SliderTemplate;
