import React, { Component } from 'react'

import Article from '../../_models/article.model'
import categories from '../../categories.json'
import FormTemplate from '../_templates/form/form.template';

export default class AddArticle extends Component {

    state = {
        formData: {
            title: {
                element: 'input',
                value: '',
                label: true,
                labelText: 'Title',
                config: {
                    name: 'title',
                    type: 'text',
                    placeholder: 'Enter your name'
                },
                validation: {
                    required: true,
                    minLen: 5
                },
                valid: false,
                touched: false,
                validationMessage: ''
            },
            urlToImage: {
                element: 'input',
                value: '',
                label: true,
                labelText: 'Url to image',
                config: {
                    name: 'urlToImage',
                    type: 'text',
                    placeholder: 'Enter your Lastname'
                },
                validation: {
                    required: true,
                    minLen: 5
                },
                valid: false,
                touched: false,
                validationMessage: ''
            },
            description: {
                element: 'textarea',
                value: '',
                label: true,
                labelText: 'Description',
                config: {
                    name: 'description',
                    rows: 4,
                    cols: 36
                },
                validation: {
                    required: true
                },
                valid: false
            },
            category: {
                element: 'select',
                value: 'all',
                label: true,
                labelText: 'Category',
                config: {
                    name: 'category',
                    options: [
                        ...categories
                    ]
                },
                validation: {
                    required: false
                },
                valid: true
            }
        },
        formIsValid: false
    }

    componentWillMount() {
        let formIsValid = true;
        for (let key in this.state.formData) {
            formIsValid = this.state.formData[key].valid && formIsValid;
        }
        this.setState({
            formIsValid
        })
    }

    changeState = (newState) => {
        let formIsValid = true;
        for (let key in this.state.formData) {
            formIsValid = this.state.formData[key].valid && formIsValid;
        }
        this.setState({
            formIsValid
        })
        this.setState({
            formData: newState
        })
    }

    showOptions = () => {

        return categories.map((category, i) => {
            return (
                <option key={i} value={category.value}>{category.name}</option>
            )
        })
    }

    onSubmitForm = (event) => {
        event.preventDefault();
        let article = new Article();
        let formIsValid = true;

        for (let key in this.state.formData) {
            article[key] = this.state.formData[key].value;
        }

        for (let key in this.state.formData) {
            formIsValid = this.state.formData[key].valid && formIsValid;
        }
        let articles = JSON.parse(localStorage.getItem('articles')) || [];
        articles.push(article);
        localStorage.setItem('articles', JSON.stringify(articles));
        this.props.history.push(`/articles/${article._id}`);
        console.log(article)
    }

    render() {
        return (
            <div className='add-article'>
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <br />
                            <h3 className="heading-large">Add article</h3>
                            <form action="" onSubmit={this.onSubmitForm}>
                                <FormTemplate
                                    formData={this.state.formData}
                                    change={this.changeState}
                                />
                                <button disabled={!this.state.formIsValid ? true : false} type='submit' className='btn btn-danger'>Add</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
