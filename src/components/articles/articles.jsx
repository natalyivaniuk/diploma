import React, { Component } from 'react';
import ArticlesList from '../_widgets/articles-list/articles-list'

import { connect } from 'react-redux'
import { articlesFetchData } from '../../_actions/articles'

class Articles extends Component {

    componentWillMount() {
        this.props.fetchData(this.props.news_category);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.news_category !== this.props.news_category) {
            this.props.fetchData(nextProps.news_category)
        }
    }

    render() {
        console.log(this.props.articles)
        return (
            <section className='p-news'>
                <div className="container">
                    < ArticlesList
                        page='articles'
                        title='Articles'
                        select='category'
                        data={this.props.articles}
                    />
                </div>
            </section>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        articles: state.articles
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (url) => {
            dispatch(articlesFetchData(url))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Articles);

