import React, { Component } from 'react';
import Header from '../header/header';
import Footer from '../footer/footer';

class BaseLayout extends Component {
    state = {
        openSideNav: false
    }
    toggleSideNav = (bool) =>{
        this.setState({
            openSideNav: bool
        })
    }

    render() {
        return (
            <div>
                < Header
                    openSideNav={this.state.openSideNav}
                    onOpenSideNav={() => this.toggleSideNav(true)}
                    onHideSideNav={() => this.toggleSideNav(false)}
                />
                <div style={{padding: '50px 0'}}>
                    {this.props.children}
                </div>
                < Footer/>
            </div>
        );
    }
}

export default BaseLayout;
