import React, { Component } from 'react';
import './articles-list.scss';
import axios from "axios";
import ArticlesListItem from './articles-list-item/articles-list-item';
import Loader from '../loader/loader';
import { connect } from 'react-redux';
import SelectWidget from '../select/select.widget';

import { newsFetchData } from '../../../_actions/news'

class ArticlesList extends Component {

    showTitle = () => {
        return (
            <h3 className="heading-large">{this.props.title}</h3>
        )
    }

    showItems = () => {
        return this.props.data.items.map((item, i) => {
            return (
                < ArticlesListItem key={i} item={item} page={this.props.page} />
            )
        })
    }

    render() {
        return (
            <div className='articles-list'>
                <div className="row articles-list__panel">
                    <div className="col-6">
                        {this.showTitle()}
                    </div>
                    <div className="col-6">
                        {(this.props.page == 'articles') ? null : <SelectWidget page={this.props.page} select={this.props.select} />}
                    </div>
                </div>
                <div className="row">
                    {this.props.data.isLoading ? <Loader /> : this.showItems()}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        news: state.news,
        news_language: state.news_language,
        news_category: state.news_category,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (url) => {
            dispatch(newsFetchData(url))
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(ArticlesList);
