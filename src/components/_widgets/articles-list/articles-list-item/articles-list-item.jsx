import React, { Component } from 'react';
import { Link } from 'react-router-dom'

import languages from '../../../../languages.json'


class ArticlesListItem extends Component {

    showImage = () => {
        const { urlToImage } = this.props.item;
        return urlToImage ? urlToImage : `http://denrakaev.com/wp-content/uploads/2015/03/no-image-800x511.png`
    }



    showDate = () => {
        const { publishedAt } = this.props.item;
        const date = new Date(publishedAt);

        let minutes = date.getMinutes();
        let month = date.getMonth() + 1;

        if (minutes < 10) {
            minutes = `0${minutes}`
        }
        if (month < 10) {
            month = `0${month}`
        }

        return `${date.getDate()}/${month}/${date.getFullYear()} ${date.getHours()}:${minutes}`;
    }

    showLanguage = () => {
        const abbr = this.props.item.language;
        let name;
        for (let item in languages) {
            if (item == abbr) {
                name = languages[item].name;
            }
        }

        return name || 'Not known';
    }

    showNews = () => {
        const { item } = this.props;
        return (
            <div className="col-md-3">
                <div className="card"> <img className="img-fluid" src={this.showImage()} alt={item.title} />
                    <div className="card-body">
                        <div className="news-title">
                            <h2 className=" title-small"><a target='_blank' href={`${item.url}`}>{item.title}</a></h2>
                        </div>
                        <p className="card-text"><small className="text-time"><em>{this.showDate()}</em></small></p>
                    </div>
                </div>
            </div>
        );
    }
    showSources = () => {
        const { item } = this.props;
        return (
            <div className="col-md-3">
                <div className="card">
                    <div className="card-img-overlay">
                        <span className="badge badge-pill badge-danger">{item.category.charAt(0).toUpperCase() + item.category.slice(1)}</span>
                    </div>
                    <div className="card-body">
                        <div className="news-title">
                            <h2 className=" title-small"><a target='_blank' href={`${item.url}`}>{item.name}</a></h2>
                        </div>
                        <div className="card__description">
                            {item.description}
                        </div>
                        <p className="card-text"><small className="text-time"><em>Language: {this.showLanguage()}</em></small></p>
                    </div>
                </div>
            </div>
        );
    }
    showArticles = () => {
        const { item } = this.props;
        return (
            <div className="col-md-3">
                <div className="card"> <img className="img-fluid" src={this.showImage()} alt={item.title} />
                    <div className="card-img-overlay">
                        <span className="badge badge-pill badge-danger">{item.category.charAt(0).toUpperCase() + item.category.slice(1)}</span>
                    </div>
                    <div className="card-body">
                        <div className="news-title">
                            <h2 className=" title-small"><Link to={`/articles/${item._id}`}>{item.title}</Link></h2>

                        </div>
                        <p className="card-text"><small className="text-time"><em>{this.showDate()}</em></small></p>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        const { page } = this.props;
        switch (page) {
            case 'news':
                return this.showNews();
            case 'sources':
                return this.showSources();
            case 'articles':
                return this.showArticles();
            default:
                return null;
        }
    }
}

export default ArticlesListItem;
