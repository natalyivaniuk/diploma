import React, { Component } from 'react';
import { connect } from 'react-redux';
import { newsFetchData } from '../../../_actions/news'

import SliderTemplate from '../../_templates/slider/slider.template';
import Loader from '../loader/loader';

class SliderWidget extends Component {


    componentWillMount() {
        this.props.fetchData(`${process.env.API_URL}/top-headlines?country=${this.props.news_country}&apiKey=${process.env.API_KEY}`)
        console.log(this.props.news_country)
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.news_country !== this.props.news_country) {
            this.props.fetchData(`${process.env.API_URL}/top-headlines?country=${nextProps.news_country}&apiKey=${process.env.API_KEY}`)
        }
    }

    render() {
        return (
            <div>
                {!this.props.news.isLoading ? <SliderTemplate data={this.props.news.items.slice(this.props.start, this.props.amount)} type={this.props.type} settings={this.props.settings} /> : <Loader />}
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        news: state.news,
        news_country: state.news_country
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (url) => {
            dispatch(newsFetchData(url))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SliderWidget);
