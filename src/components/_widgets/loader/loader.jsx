import React, { Component } from 'react';
import './loader.scss';

class Loader extends Component {
    render() {
        return (
            <div className='loader'>
                <img src="/static/images/loader.png" alt="loading..."/>
            </div>
        );
    }
}

export default Loader;
