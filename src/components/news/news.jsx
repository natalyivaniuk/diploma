import React, { Component } from 'react';
import ArticlesList from '../_widgets/articles-list/articles-list';

import { connect } from 'react-redux'
import { newsFetchData } from '../../_actions/news'

class News extends Component {

    componentWillMount() {
        this.props.fetchData(`${process.env.API_URL}/top-headlines?country=${this.props.news_country}&apiKey=${process.env.API_KEY}`)
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.news_country !== this.props.news_country) {
            this.props.fetchData(`${process.env.API_URL}/top-headlines?country=${nextProps.news_country}&apiKey=${process.env.API_KEY}`)
        }
    }


    render() {
        console.log(this.props.news)
        return (
            <section className='p-news'>
                <div className="container">
                    < ArticlesList
                        page='news'
                        title='News'
                        select='country'
                        data={this.props.news}
                    />
                </div>
            </section>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        news: state.news,
        news_country: state.news_country
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (url) => {
            dispatch(newsFetchData(url))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(News);
