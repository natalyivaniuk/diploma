import React, { Component } from 'react'

class ArticleDetail extends Component {
    state = {
        article: {}
    }

    componentWillMount = () => {
        const article = JSON.parse(localStorage.getItem('articles')).find(article => article._id == this.props.match.params.id)
        this.setState({
            article
        })
    }

    showImage = () => {
        const { urlToImage } = this.state.article;
        return urlToImage ? urlToImage : `http://denrakaev.com/wp-content/uploads/2015/03/no-image-800x511.png`
    }



    showDate = () => {
        const { publishedAt } = this.state.article;
        const date = new Date(publishedAt);

        let minutes = date.getMinutes();
        let month = date.getMonth() + 1;

        if (minutes < 10) {
            minutes = `0${minutes}`
        }
        if (month < 10) {
            month = `0${month}`
        }

        return `${date.getDate()}/${month}/${date.getFullYear()} ${date.getHours()}:${minutes}`;
    }

    render() {
        const { article } = this.state;
        return (
            <section className='article-detail'>
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <h3 className="heading-large">{article.title}</h3>
                            <div className="card"> <img className="img-fluid" src={this.showImage()} alt={article.title} />
                                <div className="card-img-overlay">
                                    <span className="badge badge-pill badge-danger">{article.category.charAt(0).toUpperCase() + article.category.slice(1)}</span>
                                </div>
                                <div className="card-body">
                                    <div className="card__description">
                                        {article.description}
                                    </div>
                                    <p className="card-text"><small className="text-time"><em>{this.showDate()}</em></small></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}
const mapStateToProps = (state) => ({
    news_category: state.news_category
})

const mapDispatchToProps = {

}

export default ArticleDetail
