import React, { Component } from 'react';
import SliderWidget from '../_widgets/slider/slider.widget';
import { Link } from 'react-router-dom'

class Home extends Component {

    state = {
        articles: []
    }

    componentWillMount = () => {
        let articles = JSON.parse(localStorage.getItem('articles')) || [];
        articles = articles.reverse().slice(0,4);
        this.setState({
            articles
        })
    }

    showArticles = () => {
        const { articles } = this.state;
        return (
            <div className="col-md-6">
                <div className="row">
                    {this.showArticlesItems()}
                </div>
            </div>
        )
    }

    showArticlesItems = () => {
        const { articles } = this.state;
        return articles.map(article => {
            return (
                <div key={article._id} className="col-md-6">
                    <div className="card"> <img className="img-fluid" src={article.urlToImage} alt={article.title} />
                        <div className="card-img-overlay"> <span className="badge badge-pill badge-danger">{article.category}</span> </div>
                        <div className="card-body">
                            <div className="news-title">
                                <h2 className=" title-small"><Link to={`/articles/${article._id}`}>{article.title}</Link></h2>
                            </div>
                            <p className="card-text"><small className="text-time"><em>{this.showDate(article.publishedAt)}</em></small></p>
                        </div>
                    </div>
                </div>
            )
        })
    }

    showDate = (published) => {
        const date = new Date(published);

        let minutes = date.getMinutes();
        let month = date.getMonth() + 1;

        if (minutes < 10) {
            minutes = `0${minutes}`
        }
        if (month < 10) {
            month = `0${month}`
        }

        return `${date.getDate()}/${month}/${date.getFullYear()} ${date.getHours()}:${minutes}`;
    }

    render() {
        return (
            <div>
                <section className="banner-sec">
                    <div className="container">
                        <div className="row">
                            {this.showArticles()}
                            <div className="col-md-6 top-slider">
                                <SliderWidget
                                    type="news"
                                    start={0}
                                    amount={3}
                                    settings={{
                                        dots: false
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default Home;
