import React, { Component } from 'react';
import SideNav from "react-simple-sidenav";
import SideNavigationItem from './side-navigation-item/side-navigation-item';

class SideNavigation extends Component {
    render() {
        return (
            <div>
                <SideNav
                    showNav={this.props.openSideNav}
                    onHideNav={this.props.onHideSideNav}
                    navStyle={{
                        background: "#212519",
                        maxWidth: "300px",
                        padding: "15px",
                        boxShadow: "2px 0 12px #222"
                    }}
                >
                    <SideNavigationItem/>
                </SideNav>
            </div>
        );
    }
}

export default SideNavigation;
