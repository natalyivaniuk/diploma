import React, { Component } from 'react';
import FontAwesome from "react-fontawesome";
import { NavLink } from "react-router-dom";


class SideNavigationItem extends Component {
    items = [
        {
            icon: 'home',
            text: 'Home',
            link: '/'
        },
        {
            icon: 'play',
            text: 'Sources',
            link: '/sources'
        },
        {
            icon: 'file-text-o',
            text: 'News',
            link: '/news'
        },
        {
            icon: 'newspaper-o',
            text: 'Articles',
            link: '/articles'
        },
        {
            icon: 'sign-in',
            text: 'Sign-in',
            link: '/sign-in'
        },
        {
            icon: 'sign-out',
            text: 'Sign-out',
            link: '/sign-out'
        },
    ]

    showItems = () => {
        return this.items.map(item => {
            return (
                <div key={item.link} className="sideNav-item">
                    < NavLink to={item.link} exact activeClassName="active" >
                        <FontAwesome name={item.icon} />
                        {item.text}
                    </NavLink>
                </div>
            )
        })
    }

    render() {
        return (
            <div>
                {this.showItems()}
            </div>
        );
    }
}

export default SideNavigationItem;
