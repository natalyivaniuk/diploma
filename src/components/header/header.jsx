import React, { Component } from 'react';
import { NavLink } from "react-router-dom";
import FontAwesome from "react-fontawesome";
import SideNavigation from './side-navigation/side-navigation';
import SelectWidget from '../_widgets/select/select.widget';

class Header extends Component {
    sideBars = () => {
        return (
            <div className="burger-btn">
                <FontAwesome name="bars" onClick={this.props.onOpenSideNav} />
            </div>
        )
    }
    render() {
        return (
            <div>
                <div id="Date"></div>
                <div className="small-top">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-4 date-sec">
                            </div>
                            <div className="col-lg-3 offset-lg-5">
                                <div className="social-icon"> <a target="_blank" href="#" className=" fa fa-facebook"></a> <a target="_blank" href="#" className=" fa fa-twitter"></a> <a target="_blank" href="#" className=" fa fa-google-plus"></a> <a target="_blank" href="#" className=" fa fa-linkedin"></a> <a target="_blank" href="#" className=" fa fa-youtube"></a> <a target="_blank" href="#" className=" fa fa-vimeo-square"></a> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="top-head left">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6 col-lg-4">
                                <h1>Grafreez News<small>Get the latest News</small></h1>
                            </div>
                        </div>
                    </div>
                </div>
                <section className="top-nav">
                    <nav className="navbar navbar-expand-lg py-0">
                        <div className="container">
                            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"></span>
                            </button>
                            <div className="collapse navbar-collapse" id="exCollapsingNavbar2">
                                <ul className="nav navbar-nav ">
                                    <li className="nav-item"> <NavLink exact activeClassName="active" className="nav-link" to="/">Home <span className="sr-only">(current)</span></NavLink> </li>
                                    <li className="nav-item"> <NavLink activeClassName="active" className="nav-link" to="/sources">Sources</NavLink> </li>
                                    <li className="nav-item"> <NavLink activeClassName="active" className="nav-link" to="/news">News</NavLink> </li>
                                    <li className="nav-item"> <NavLink activeClassName="active" className="nav-link" to="/articles">Articles</NavLink> </li>
                                </ul>
                                <div className="burger">
                                    <SideNavigation
                                        {...this.props}
                                    />
                                    {this.sideBars()}
                                </div>
                                <div className="header-select" style={{paddingLeft: '20px'}}>
                                    <SelectWidget select='country' />
                                </div>
                            </div>
                        </div>
                    </nav>
                </section>
            </div >
        );
    }
}

export default Header;
