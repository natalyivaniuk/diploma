import React, { Component } from 'react';
import ArticlesList from '../_widgets/articles-list/articles-list';
import { connect } from 'react-redux'
import { sourcesFetchData } from '../../_actions/sources'

class Sources extends Component {

    setUrl = (props) => {
        switch (props.news_category) {
            case 'all':
                return `${process.env.API_URL}/sources?apiKey=${process.env.API_KEY}`
            default:
                return `${process.env.API_URL}/sources?category=${props.news_category}&apiKey=${process.env.API_KEY}`
        }
    }


    componentWillMount() {
        this.props.fetchData(this.setUrl(this.props))
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.news_category !== this.props.news_category) {
            this.props.fetchData(this.setUrl(nextProps))
        }
    }


    render() {
        console.log(this.props.sources)
        return (
            <section className='p-sources'>
                <div className="container">
                    < ArticlesList
                        page='sources'
                        title='Sources'
                        select='category'
                        data={this.props.sources}
                    />
                </div>
            </section>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        sources: state.sources,
        news_category: state.news_category
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (url) => {
            dispatch(sourcesFetchData(url))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Sources);
