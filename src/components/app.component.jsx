import React, { Component } from 'react';
import './app.component.scss';
import { BrowserRouter } from "react-router-dom";
import Routers from './routers';


class AppComponent extends Component {

    render() {
        return (
            <BrowserRouter>
                <Routers />
            </BrowserRouter>
        )
    }
}

export default AppComponent;

