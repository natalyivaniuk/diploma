import { combineReducers } from 'redux';

import news_country from './news-country';
import news_category from './news-category';
import { news } from './news'
import { sources } from './sources'
import { articles } from './articles'

const reducer = combineReducers({
    news_country,
    news_category,
    news,
    sources,
    articles
});

export default reducer;
