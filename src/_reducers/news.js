// export const newsHasErrored = (state = false, action) => {
//     switch (action.type) {
//         case 'NEWS_HAS_ERRORED':
//             return action.hasErrored
//         default:
//             return state
//     }
// }
// export const newsIsLoading = (state = false, action) => {
//     switch (action.type) {
//         case 'NEWS_IS_LOADING':
//             return action.isLoading
//         default:
//             return state
//     }
// }
// export const news = (state = [], action) => {
//     switch (action.type) {
//         case 'NEWS_FETCH_DATA_SUCCESS':
//             return action.news
//         default:
//             return state
//     }
// }

const initialState = {
    items: [],
    isLoading: false,
    hasErrored: false
}

export const news = (state = initialState, action) => {
    switch (action.type) {
        case 'NEWS_HAS_ERRORED':
            return {
                ...state,
                items: [],
                isLoading: false,
                hasErrored: true
            }
        case 'NEWS_IS_LOADING':
            return {
                ...state,
                isLoading: true,
                hasErrored: false
            }
        case 'NEWS_FETCH_DATA_SUCCESS':
            return {
                ...state,
                items: action.items,
                isLoading: false
            }
        default:
            return state
    }
}
