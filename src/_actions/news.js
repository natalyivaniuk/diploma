export const newsHasErrored = () => {
    return {
        type: 'NEWS_HAS_ERRORED'
    }
}

export const newsIsLoading = () => {
    return {
        type: 'NEWS_IS_LOADING'
    }
}

export const newsFetchDataSuccess = (items) => {
    return {
        type: 'NEWS_FETCH_DATA_SUCCESS',
        items
    }
}

export const newsFetchData = url => dispatch => {
    dispatch(newsIsLoading())

    fetch(url)
        .then(res => {

            if (!res.ok) {
                throw Error(res.statusText)
            }

            return res;
        })
        .then(res => res.json())
        .then(news => dispatch(newsFetchDataSuccess(news.articles)))
        .catch(() => newsHasErrored())
}
