export const sourcesHasErrored = () => {
    return {
        type: 'SOURCES_HAS_ERRORED'
    }
}

export const sourcesIsLoading = () => {
    return {
        type: 'SOURCES_IS_LOADING'
    }
}

export const sourcesFetchDataSuccess = items => {
    return {
        type: 'SOURCES_FETCH_DATA_SUCCESS',
        items
    }
}

export const sourcesFetchData = url => dispatch => {
    dispatch(sourcesIsLoading())

    fetch(url)
        .then(res => {

            if (!res.ok) {
                throw Error(res.statusText)
            }

            return res;
        })
        .then(res => res.json())
        .then(sources => dispatch(sourcesFetchDataSuccess(sources.sources)))
        .catch(() => sourcesHasErrored())
}

