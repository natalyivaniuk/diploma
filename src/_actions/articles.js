export const articlesHasErrored = () => {
    return {
        type: 'ARTICLES_HAS_ERRORED'
    }
}

export const articlesIsLoading = () => {
    return {
        type: 'ARTICLES_IS_LOADING'
    }
}

export const articlesFetchDataSuccess = (items) => {
    return {
        type: 'ARTICLES_FETCH_DATA_SUCCESS',
        items
    }
}

export const articlesFetchData = url => dispatch => {
    dispatch(articlesIsLoading())

    const articles = JSON.parse(localStorage.getItem('articles')).reverse();

    dispatch(articlesFetchDataSuccess(articles))

    console.log(articles)


}

